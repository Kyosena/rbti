from django.db import models

class Buku(models.Model):
    id_buku = models.CharField(primary_key=True, max_length=255)
    judul = models.CharField(null=True,max_length=255)
    penulis = models.CharField(null=True,max_length=255)
    penerbit = models.CharField(null=True,max_length=255)
    tahun = models.CharField(null=True,max_length=255)
    class Meta:
        managed = True
        db_table = 'rbti_app_buku'
    def __str__(self):
        return self.id_buku

class Mahasiswa(models.Model):
    npm = models.BigIntegerField(primary_key=True)
    nama = models.CharField(null=False, max_length=255)
    email = models.CharField(null=True, max_length=255)

class Peminjaman(models.Model):
    tanggal_peminjaman=models.DateField(auto_now_add=True)
    tanggal_pengembalian=models.DateField(null=True)
    npm=models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    id_buku=models.ForeignKey(Buku, on_delete=models.CASCADE)

class Admin(models.Model):
    username = models.CharField(max_length=255, primary_key = True)
    password = models.CharField(max_length=255)