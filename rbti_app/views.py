from django.shortcuts import render,redirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic import ListView,CreateView
from django.shortcuts import get_object_or_404
from .models import *
from .forms import PeminjamanForm
import datetime
from django.db import connection, DatabaseError, transaction
import json as simplejson
# Create your views here.

def newhome(request):
    Cursor = connection.cursor()
    Cursor.execute("SELECT * FROM rbti_app_buku WHERE id_buku NOT IN (SELECT id_buku_id FROM rbti_app_peminjaman)")
    books = Cursor.fetchall()
    Cursor.execute("SELECT id_buku_id FROM rbti_app_peminjaman")
    no_books = Cursor.fetchall()
    print(no_books)
    args = {
    'books' : books
    }
    return render(request,"rbti_app/newhome.html",args)
# class BukuListView(ListView):
#     model = Buku
#     template_name = 'rbti_app/newhome.html'
#     context_object_name = 'books'
#     ordering = ['id_buku']


# class BukuListViewAlt(ListView):
#     model = Buku
#     template_name = 'rbti_app/home.html'
#     context_object_name = 'books'
#     ordering = ['id_buku']
# class PeminjamanCreateView(CreateView):
#     model = Peminjaman
#     fields = ["id_buku","npm"]
#     def dispatch(self, request, *args, **kwargs):
#         self.id_buku_id = get_object_or_404(Buku, pk=kwargs['id_buku'])
#         print("!!!!!!!!!!!! ID BUKU ADALAH {idbuku} !!!!!!!!!!!!! ".format(idbuku=self.id_buku_id))
#         return super().dispatch(request, *args, **kwargs)
#     def form_valid(self, form):
#         form.instance.id_buku_id=self.id_buku_id
#         form.instance.tanggal_peminjaman=datetime.datetime.now()
#         return super().form_valid(form)
#     def get_context_data(self, **kwargs):
#         data = super(PeminjamanCreateView, self).get_context_data(**kwargs)
#         data['id_buku'] = Buku.objects.get(pk=self.id_buku_id)
#         data['judul'] = Buku.objects.get(pk=self.id_buku_id).judul
#         return data

        # self.id_buku_id = get_object_or_404(Buku, pk=kwargs['id_buku'])
    


def peminjaman(request):
    peminjamans = Peminjaman.objects.all().filter(tanggal_pengembalian__isnull=True)
    print(peminjamans)
    args = {
        'peminjamans' : peminjamans
    }
    return render(request,"rbti_app/peminjaman.html",args)

def create_peminjaman(request,id_buku):
    Cursor = connection.cursor()
    if(request.method == 'POST'):
        nim=request.POST.get("NIM")
        print(nim)
        id_buku_list = request.POST.getlist("id_buku_form")
        last_peminjaman = Peminjaman.objects.last()
        if(last_peminjaman == None):
            id=0
        else:
            id = last_peminjaman.id + 1
        Cursor.execute("INSERT INTO rbti_app_peminjaman VALUES (%s,%s,%s,%s,%s)",[id,datetime.datetime.now(),None,id_buku,nim])
        id += 1
        print(id_buku_list)
        for id_buku_object in id_buku_list:
            try:
                print("ID BUKU : " + id_buku_object)
                current_book = Buku.objects.get(id_buku=id_buku_object)
                Cursor.execute("INSERT INTO rbti_app_peminjaman VALUES (%s,%s,%s,%s,%s)",[id,datetime.datetime.now(),None,id_buku_object,nim])
                id += 1
            except:
                message = "Oops! ID invalid for book {0}".format(id_buku_object)
                args = {
                    'message': message
                }
                return render(request,"rbti_app/peminjaman_form.html",args)
    if(id_buku == "0"):
        return render(request,"rbti_app/peminjaman_form.html")
    else:
        try:
            Cursor.execute("SELECT * FROM rbti_app_buku WHERE id_buku=%s",[id_buku])
            book = Cursor.fetchone()
            if(book == None):
                raise Exception()
            args ={
                'book' : book
            }
        except:
            args ={
                'message' : "Something went wrong! Please try a different book."
            }
        finally:
            return render(request,"rbti_app/peminjaman_form.html",args)
def ajax(request,npm):
    print("MASUK VIEW")
    args={
        'success':True,
    }
    mahasiswa = Mahasiswa.objects.get(npm=npm)
    print("HEYYY " + str(mahasiswa.npm))
    args = {
        str(mahasiswa.npm) +";",
        mahasiswa.nama + ";",
        mahasiswa.email + ";"
    }
    return HttpResponse(args)



    

