from django.urls import path
from .views import *
app_name="rbti_app"
urlpatterns = [
    path('', newhome, name='home'),
    path('peminjaman/<id_buku>',create_peminjaman, name = "create_peminjaman"),
    path('peminjaman/',create_peminjaman, name = "create_peminjaman"),
    path('list_peminjaman/', peminjaman, name = "peminjaman"),
    path('peminjaman/ajax/<npm>',ajax,name="ajax"),
    path('peminjaman/ajax/',ajax,name="ajax")
]
