# Generated by Django 3.1.3 on 2021-01-21 14:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('username', models.CharField(max_length=255, primary_key=True, serialize=False)),
                ('password', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Buku',
            fields=[
                ('id_buku', models.CharField(max_length=255, primary_key=True, serialize=False)),
                ('judul', models.CharField(max_length=255)),
                ('penulis', models.CharField(max_length=255)),
                ('penerbit', models.CharField(max_length=255)),
                ('tahun', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'rbti_app_buku',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Mahasiswa',
            fields=[
                ('npm', models.BigIntegerField(primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Peminjaman',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tanggal_peminjaman', models.DateField(auto_now_add=True)),
                ('tanggal_pengembalian', models.DateField(null=True)),
                ('id_buku', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rbti_app.buku')),
                ('npm', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rbti_app.mahasiswa')),
            ],
        ),
    ]
